const relativePathLadder = require('./relativePathLadder');

const data = {
  root: {
    filePath: '/Users/merry/Documents/Sites/gcol/src/site/index.html',
    projectRoot: '/Users/merry/Documents/Sites/gcol/src/site',
    relative: 'index.html',
    ladder: '',
  },
  two: {
    filePath: '/Users/merry/Documents/Sites/gcol/src/site/two/index2.html',
    projectRoot: '/Users/merry/Documents/Sites/gcol/src/site',
    relative: 'two/index2.html',
    ladder: '../',
  },
  three: {
    filePath: '/Users/merry/Documents/Sites/gcol/src/site/two/three/index3.html',
    projectRoot: '/Users/merry/Documents/Sites/gcol/src/site',
    relative: 'two/three/index3.html',
    ladder: '../../',
  },
  four: {
    filePath: '/Users/merry/Documents/Sites/gcol/src/site/two/three/four/index4.html',
    projectRoot: '/Users/merry/Documents/Sites/gcol/src/site',
    relative: 'two/three/four/index4.html',
    ladder: '../../../',
  },
}

test(
  `Feeding file.root will return {
    'relative': ${data.root.relative},
    'ladder': ${data.root.ladder}
  }`,
  () => {
    expect(relativePathLadder(data.root.filePath, data.root.projectRoot))
      .toStrictEqual({
        'relative': data.root.relative,
        'ladder': data.root.ladder
      });
  }
);

test(
  `Feeding file.two will return {
    'relative': ${data.two.relative},
    'ladder': ${data.two.ladder}
  }`,
  () => {
    expect(relativePathLadder(data.two.filePath, data.two.projectRoot))
      .toStrictEqual({
        'relative': data.two.relative,
        'ladder': data.two.ladder
      });
  }
);

test(
  `Feeding file.three will return {
    'relative': ${data.three.relative},
    'ladder': ${data.three.ladder}
  }`,
  () => {
    expect(relativePathLadder(data.three.filePath, data.three.projectRoot))
      .toStrictEqual({
        'relative': data.three.relative,
        'ladder': data.three.ladder
      });
  }
);

test(
  `Feeding file.four will return {
    'relative': ${data.four.relative},
    'ladder': ${data.four.ladder}
  }`,
  () => {
    expect(relativePathLadder(data.four.filePath, data.four.projectRoot))
      .toStrictEqual({
        'relative': data.four.relative,
        'ladder': data.four.ladder
      });
  }
);
