"use strict";

const del = require('del');

/**
 * Clean folder and/or files
 * @param {Object} data - Cleaning configuration
 * @param {String|Object} data.paths The paths to delete
 */

module.exports = (data) => {

  const clean = () => {
    return del(data.paths);
  }

  return clean;

};
