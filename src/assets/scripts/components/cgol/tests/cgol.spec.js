import get2DArrMax from '../../../utilities/get2DArrMax';
import { ALIVE, DEAD, CONTROLLER_NAME_LIST } from '../config';
import CGOL from '../cgol';

describe(`Conway's Game of Life`, () => {

  const URL = 'http://localhost:3000/assets/scripts/components/cgol/tests/index.html';
  
  context(`Game logic`, () => {
    let cgol;

    const defaultCellList = [
      { pos: { col: 0, row: 0 }, state: DEAD },
      { pos: { col: 0, row: 1 }, state: DEAD },
      { pos: { col: 0, row: 2 }, state: DEAD },
      { pos: { col: 1, row: 0 }, state: DEAD },
      { pos: { col: 1, row: 1 }, state: DEAD },
      { pos: { col: 1, row: 2 }, state: DEAD },
      { pos: { col: 2, row: 0 }, state: DEAD },
      { pos: { col: 2, row: 1 }, state: DEAD },
      { pos: { col: 2, row: 2 }, state: DEAD }
    ];

    beforeEach(() => {
      cy.visit(URL);
      cgol = new CGOL();
    });

    describe(`Create an area containing x number of cells`, () => {
      it(`Should have 9 non-alive cells`, () => {
        expect(cgol.area).to.deep.equal(defaultCellList);
      });
    });

    describe(`Get total number of live cells`, () => {
      it(`Should be 0`, () => {
        expect(cgol.getTotalNumberOfLives()).to.equal(0);
      });
    });

    describe(`Get cell index from the list`, () => {
      it(`Should return cell index`, () => {
        expect(cgol.getCellIndex(0, 0)).to.equal(0);
      });
    });

    describe(`Bring a cell to life`, () => {
      it(`Cell located at 1,1 should be alive`, () => {
        cgol.setCellState(0, 0, ALIVE);
        expect(cgol.getCellState(0, 0)).to.equal(ALIVE);
      });
    });

    describe(`Remove a life from a specific cell`, () => {
      it(`Cell located at 1,1 should not be alive`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(0, 0, DEAD);
        expect(cgol.getCellState(0, 0)).to.equal(DEAD);
      });
    });

    describe(`Populate the area with x number of lives at random cells`, () => {
      it(`Should have 6 living cells`, () => {
        expect(cgol.getTotalNumberOfLives()).to.equal(0);
        cgol.bringRandomCellsToLife(3);
        expect(cgol.getTotalNumberOfLives()).to.equal(3);
      });
    });

    describe(`Retrieve specific pattern from pattern list`, () => {
      const
        miniInvadersPatternCells = [
          [0, 2], [1, 1], [2, 2],
        ],
        minColSize = 3,
        minRowSize = 3;

      it(`Should return Mini Invaders pattern`, () => {
        expect(CGOL.getPattern('Mini Invaders').pattern(CGOL).cells).to.deep.equal(miniInvadersPatternCells);
      });

      it(`Should return pattern's minimum column size`, () => {
        const miniInvaders = CGOL.getPattern('Mini Invaders').pattern(CGOL);
        expect(miniInvaders.minColSize).to.equal(minColSize);
      });

      it(`Should return pattern's minimum row size`, () => {
        const miniInvaders = CGOL.getPattern('Mini Invaders').pattern(CGOL);
        expect(miniInvaders.minRowSize).to.equal(minRowSize);
      });
    });

    describe(`Should bring cells to life according to specified pattern`, () => {
      it(`Area population should reflect Mini Invaders pattern`, () => {
        cgol.setAreaPattern('Mini Invaders');

        expect(cgol.getCellState(0, 2)).to.equal(ALIVE);
        expect(cgol.getCellState(1, 1)).to.equal(ALIVE);
        expect(cgol.getCellState(2, 2)).to.equal(ALIVE);
      });
    });

    describe(`Wipe out all life`, () => {
      it(`Total number of lives should be 0`, () => {
        cgol.bringRandomCellsToLife(4);
        cgol.wipeOutAllLife();
        expect(cgol.getTotalNumberOfLives()).to.equal(0);
      });
    });

    describe(`Get total number of live neighbour`, () => {
      it(`Should have 3 living neighbours`, () => {
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(2, 0, ALIVE);

        expect(cgol.getTotalLivingNeighbours(1, 1)).to.equal(3);
      });

      it(`Should still have 3 living neighbours even when it's at the edge`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(0, 2, ALIVE);

        expect(cgol.getTotalLivingNeighbours(0, 0)).to.equal(3);
      });
    });

    describe(`Get specific cell next state based on the rules`, () => {
      it(`Cell becomes dead due to underpopulation since it only had less than two live neighbours`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 0, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).to.equal(DEAD);
        expect(cgol.getCellNextFate(0, 0).reason).to.equal('underpopulation');
      });

      it(`Cell lives on since it had two live neighbours`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 1, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).to.equal(ALIVE);
        expect(cgol.getCellNextFate(0, 0).reason).to.equal('lives on');
      });

      it(`Cell also lives on since it had three live neighbours`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 2, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 1, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).to.equal(ALIVE);
        expect(cgol.getCellNextFate(0, 0).reason).to.equal('lives on');
      });

      it(`Cell becomes dead due to overpopulation since it had more than three live neighbours`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 2, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 1, ALIVE);
        cgol.setCellState(2, 1, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).to.equal(DEAD);
        expect(cgol.getCellNextFate(0, 0).reason).to.equal('overpopulation');
      });

      it(`Cell becomes alive due to reproduction since it had exactly 3 live neighbours`, () => {
        cgol.setCellState(1, 2, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 1, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).to.equal(ALIVE);
        expect(cgol.getCellNextFate(0, 0).reason).to.equal('reproduction');
      });

      it(`Cell stays dead due to unideal neighbours`, () => {
        cgol.setCellState(1, 2, ALIVE);
        cgol.setCellState(1, 0, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).to.equal(DEAD);
        expect(cgol.getCellNextFate(0, 0).reason).to.equal('unideal neighbours');
      });
    });

    describe(`Create a new area with the new states`, () => {
      it(`Should not create future cell list if area is empty`, () => {
        const futureArea = cgol.getFutureCellList();
        expect(futureArea).to.be.undefined;
      });

      it(`Population becomes 0`, () => {
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(1, 0, ALIVE);

        const futureArea = cgol.getFutureCellList();

        expect(futureArea.length).to.equal(9);
        expect(cgol.getTotalNumberOfLives(futureArea)).to.equal(0);
      });

      it(`Population becomes 9`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(1, 0, ALIVE);

        const futureArea = cgol.getFutureCellList();

        expect(futureArea.length).to.equal(9);
        expect(cgol.getTotalNumberOfLives(futureArea)).to.equal(9);
      });
    });

    describe(`Start timelapse`, () => {
      it(`Only when it has a future`, () => {
        cgol.setCellState(2, 0, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 0, ALIVE);

        let
          area = cgol.area,
          futureArea = cgol.getFutureCellList();

        expect(futureArea).to.not.be.undefined;

        const hasFuture = cgol.checkFutureCondition(area, futureArea);
        expect(hasFuture).to.be.true;
      });

    });
  });

  context(`Game visual`, () => {
    let cgol;

    const 
      id = 'universe',
      width = 90,
      height = 90;

    beforeEach(() => {
      cy.visit(URL);

      const canvas = document.createElement('canvas');
      canvas.id = id;

      // Canvas with same ID must also exist at http://localhost:3000/
      // alongside this DOM below
      document.body.appendChild(canvas);

      cgol = new CGOL({
        id: id,
        width: width,
        height: height,
        dev: true,
      });

      cgol.setCellState(0, 0, ALIVE);
      cgol.setCellState(1, 1, ALIVE);
      cgol.setCellState(0, 2, ALIVE);
      cgol.setCellState(7, 7, ALIVE);
      cgol.setCellState(8, 7, ALIVE);
    });

    beforeEach(() => {
      cy.get(`canvas#${id}`)
        .as('canvas');

      // cgol.setupCanvas();
      // cgol.drawArea();

      CONTROLLER_NAME_LIST.forEach(controllerName => {
        cy.get(`[data-cgol-controller="${controllerName}"]`).as(`controller-${controllerName}`);
      });
    });

    describe(`Canvas setup`, () => {
      it(`canvas#${id} should exist`, () => {
        cy.get('@canvas');

        cy.wrap(cgol.canvas)
          .should('have.attr', 'id', id);
      });

      it(`Its dimension should be the same as the defined values at instantiation`, () => {
        cy.wrap(cgol.canvas)
          .should('have.attr', 'width', width.toString())
          .should('have.attr', 'height', height.toString());

        cy.get('@canvas')
          .should('have.attr', 'width', width.toString())
          .should('have.attr', 'height', height.toString());
      });

      it(`Cells are drawn on canvas`, () => {
        // cy.percySnapshot(`canvas#universe`);
      });
      
      it(`Clears canvas`, () => {
        // cy.percySnapshot(`clear canvas#universe`);
      });
    });

    describe(`Control room`, () => {
      it(`It exists`, () => {
        cy.get(`[data-cgol-control-room='${id}']`);
      });

      let winLog;

      Cypress.on('window:before:load', (win) => {
        winLog = cy.spy(win.console, "log");
      });

      it(`Create controllers`, () => {
        CONTROLLER_NAME_LIST.forEach(controllerName => {
          cy.get(`@controller-${controllerName}`)
            .click();
          // cy.percySnapshot(`canvas#universe forward #1`);
        });
      });

      it(`Should toggle timelapse to start or pause`, () => {
        cy.get(`@controller-toggleTimelapse`)
          .invoke('attr', 'data-cgol-action')
          .should('eq', '');
        
        cy.get(`@controller-toggleTimelapse`)
          .click()
          .invoke('attr', 'data-cgol-action')
          .should('eq', 'pause');
        
        cy.get(`@controller-toggleTimelapse`)
          .click()
          .invoke('attr', 'data-cgol-action')
          .should('eq', 'play');
      });

      it(`Draws next area when forward button is clicked`, () => {
        expect(cgol.getCellState(0, 0)).to.equal(ALIVE);
        expect(cgol.getCellState(1, 1)).to.equal(ALIVE);
        expect(cgol.getCellState(0, 2)).to.equal(ALIVE);
        expect(cgol.getCellState(7, 7)).to.equal(ALIVE);
        expect(cgol.getCellState(8, 7)).to.equal(ALIVE);

        cgol.forward();

        // Updated initial cells
        expect(cgol.getCellState(0, 0)).to.equal(DEAD);
        expect(cgol.getCellState(1, 1)).to.equal(ALIVE);
        expect(cgol.getCellState(0, 2)).to.equal(DEAD);
        expect(cgol.getCellState(7, 7)).to.equal(DEAD);
        expect(cgol.getCellState(8, 7)).to.equal(DEAD);
        // New cells
        expect(cgol.getCellState(0, 1)).to.equal(ALIVE);
        expect(cgol.getCellState(8, 8)).to.equal(ALIVE);
      });
    });
  });
});