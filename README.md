# Conway's Game of Life

Created this after I participated in Global Day of Code Retreat that was held in Singapore on November 16, 2019. The event was organised by [Junior Developers Singapore](https://www.meetup.com/Junior-Developers-Singapore/events/264498735/) meetup group. 

The main agenda for the event was to exercise Test Driven Development (TDD) and pair programming. It was my first time doing TDD and full day of pair programming. 

Glad to have paired with these people who willingly shared their knowledge and provided guidance:
* [angrylobster](https://github.com/angrylobster)
* Hui Min
* Stan

More information about the event can be found here at [coderetreat.org](https://www.coderetreat.org/)
