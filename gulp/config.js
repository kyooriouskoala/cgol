"use strict";

const argv = require('minimist')(process.argv.slice(2));

/**
 * Gulp configuration.
 * Edit only if you know what you are doing.
 */

const config = {
  buildDir: argv._[0] === 'prod' ? 'public' : 'dev',
  browserSync: {
    name: 'CGOL (*・‿・)ノ⌒*:･ﾟ✧'
  },
  isProduction: argv._[0] === 'prod',
  projectRoot: __dirname.replace('/gulp', ''),
  template: {
    engine: 'pug'
  }
}

module.exports = config;
