"use stricts";

const gulpConfig = require('../config');

const
  $_ = {
    if: require('gulp-if'),
    rename: require('gulp-rename'),
    sourcemaps: require('gulp-sourcemaps'),
    uglify: require('gulp-uglify'),
  },
  babelify = require('babelify'),
  buffer = require('vinyl-buffer'),
  browserify = require('browserify'),
  gulp = require('gulp'),
  source = require('vinyl-source-stream');

/**
 * Handle scripts for different builds
 */

const scripts = (done) => {
  let jsFiles = [
    'main.js',
    'components/cgol/tests/demo.js',
  ];

  jsFiles.map((entry) => {
    return browserify({
      entries: ['src/assets/scripts/' + entry]
    })
      .transform(babelify, {
        presets: ['@babel/preset-env']
      })
      .bundle()
      .pipe(source(entry))
      .pipe($_.if(
        gulpConfig.isProduction,
        $_.rename({ extname: '.min.js' })
      ))
      .pipe(buffer())
      .pipe($_.sourcemaps.init({ loadMaps: true }))
      .pipe($_.if(
        gulpConfig.isProduction,
        $_.uglify()
      ))
      .pipe($_.if(
        !gulpConfig.isProduction,
        $_.sourcemaps.write('./')
      ))
      .pipe(gulp.dest('./' + gulpConfig.buildDir + '/assets/scripts/'));
  });

  done();
}

module.exports = scripts;