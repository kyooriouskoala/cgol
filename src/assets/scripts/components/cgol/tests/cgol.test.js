import get2DArrMax from '../../../utilities/get2DArrMax';
import { ALIVE, DEAD } from '../config';
import CGOL from '../cgol';

describe(`Conway's Game of Life`, () => {
  describe(`Game logic`, () => {
    let cgol;

    const defaultCellList = [
      { pos: { col: 0, row: 0 }, state: DEAD },
      { pos: { col: 0, row: 1 }, state: DEAD },
      { pos: { col: 0, row: 2 }, state: DEAD },
      { pos: { col: 1, row: 0 }, state: DEAD },
      { pos: { col: 1, row: 1 }, state: DEAD },
      { pos: { col: 1, row: 2 }, state: DEAD },
      { pos: { col: 2, row: 0 }, state: DEAD },
      { pos: { col: 2, row: 1 }, state: DEAD },
      { pos: { col: 2, row: 2 }, state: DEAD }
    ];

    beforeEach(() => {
      cgol = new CGOL({
        preset: false,
      });
    });

    describe(`Create an area containing x number of cells`, () => {
      test(`Should have 9 non-alive cells`, () => {
        expect(cgol.area).toEqual(defaultCellList);
      });
    });

    describe(`Get total number of live cells`, () => {
      test(`Should be 0`, () => {
        expect(cgol.getTotalNumberOfLives()).toEqual(0);
      });
    });

    describe(`Get cell index from the list`, () => {
      test(`Should return cell index`, () => {
        expect(cgol.getCellIndex(0, 0)).toEqual(0);
      });
    });

    describe(`Bring a cell to life`, () => {
      test(`Cell located at 1,1 should be alive`, () => {
        cgol.setCellState(0, 0, ALIVE);
        expect(cgol.getCellState(0, 0)).toEqual(ALIVE);
      });
    });

    describe(`Remove a life from a specific cell`, () => {
      test(`Cell located at 1,1 should not be alive`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(0, 0, DEAD);
        expect(cgol.getCellState(0, 0)).toEqual(DEAD);
      });
    });

    describe(`Populate the area with x number of lives at random cells`, () => {
      test(`Should have 6 living cells`, () => {
        expect(cgol.getTotalNumberOfLives()).toEqual(0);
        cgol.bringRandomCellsToLife(3);
        expect(cgol.getTotalNumberOfLives()).toEqual(3);
      });
    });

    describe(`Wipe out all life`, () => {
      test(`Total number of lives should be 0`, () => {
        cgol.bringRandomCellsToLife(4);
        cgol.wipeOutAllLife();
        expect(cgol.getTotalNumberOfLives()).toEqual(0);
      });
    });

    describe(`Get total number of live neighbour`, () => {
      test(`Should have 3 living neighbours`, () => {
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(2, 0, ALIVE);

        expect(cgol.getTotalLivingNeighbours(1, 1)).toEqual(3);
      });

      test(`Should still have 3 living neighbours even when it's at the edge`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(0, 2, ALIVE);

        expect(cgol.getTotalLivingNeighbours(0, 0)).toEqual(3);
      });
    });

    describe(`Get specific cell next state based on the rules`, () => {
      test(`Cell becomes dead due to underpopulation since it only had less than two live neighbours`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 0, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).toEqual(DEAD);
        expect(cgol.getCellNextFate(0, 0).reason).toEqual('underpopulation');
      });

      test(`Cell lives on since it had two live neighbours`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 1, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).toEqual(ALIVE);
        expect(cgol.getCellNextFate(0, 0).reason).toEqual('lives on');
      });

      test(`Cell also lives on since it had three live neighbours`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 2, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 1, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).toEqual(ALIVE);
        expect(cgol.getCellNextFate(0, 0).reason).toEqual('lives on');
      });

      test(`Cell becomes dead due to overpopulation since it had more than three live neighbours`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 2, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 1, ALIVE);
        cgol.setCellState(2, 1, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).toEqual(DEAD);
        expect(cgol.getCellNextFate(0, 0).reason).toEqual('overpopulation');
      });

      test(`Cell becomes alive due to reproduction since it had exactly 3 live neighbours`, () => {
        cgol.setCellState(1, 2, ALIVE);
        cgol.setCellState(1, 0, ALIVE);
        cgol.setCellState(0, 1, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).toEqual(ALIVE);
        expect(cgol.getCellNextFate(0, 0).reason).toEqual('reproduction');
      });

      test(`Cell stays dead due to unideal neighbours`, () => {
        cgol.setCellState(1, 2, ALIVE);
        cgol.setCellState(1, 0, ALIVE);

        expect(cgol.getCellNextFate(0, 0).state).toEqual(DEAD);
        expect(cgol.getCellNextFate(0, 0).reason).toEqual('unideal neighbours');
      });
    });

    describe(`Create a new area with the new states`, () => {
      test(`Population becomes 0`, () => {
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(1, 0, ALIVE);

        const futureArea = cgol.createFutureCellList();

        expect(futureArea.length).toEqual(9);
        expect(cgol.getTotalNumberOfLives(futureArea)).toEqual(0);
      });

      test(`Population becomes 9`, () => {
        cgol.setCellState(0, 0, ALIVE);
        cgol.setCellState(1, 1, ALIVE);
        cgol.setCellState(1, 0, ALIVE);

        const futureArea = cgol.createFutureCellList();

        expect(futureArea.length).toEqual(9);
        expect(cgol.getTotalNumberOfLives(futureArea)).toEqual(9);
      });
    });

    describe(`Retrieve specific pattern from pattern list`, () => {
      test(`Should return Mini Invaders pattern`, () => {
        const miniInvadersPattern = {
          name: 'Mini Invaders',
          cells: [
            [0, 2],
            [1, 1],
            [2, 2],
          ]
        }

        expect(CGOL.getPattern('Mini Invaders')).toEqual(miniInvadersPattern);
      });

      test(`Should return pattern's minimum column size`, () => {
        const miniInvaders = CGOL.getPattern('Mini Invaders');
        expect(get2DArrMax(miniInvaders.cells, 0)).toEqual(2);
      });

      test(`Should return pattern's minimum row size`, () => {
        const miniInvaders = CGOL.getPattern('Mini Invaders');
        expect(get2DArrMax(miniInvaders.cells, 0)).toEqual(2);
      });
    });

    describe(`Should bring cells to life according to specified pattern`, () => {
      test(`Area population should reflect Mini Invaders pattern`, () => {
        cgol.setAreaPattern('Mini Invaders');

        expect(cgol.getCellState(0, 2)).toEqual(ALIVE);
        expect(cgol.getCellState(1, 1)).toEqual(ALIVE);
        expect(cgol.getCellState(2, 2)).toEqual(ALIVE);
      });
    });
  });
});