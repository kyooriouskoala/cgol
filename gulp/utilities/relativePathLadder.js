"use strict";

/**
 * Get file relative path and path ladder
 * @param {String} filePath File path
 * @param {String} projectRoot Project root path
 * @returns {Object} The file relative path and the path ladder
 */

const relativePathLadder = (filePath, projectRoot) => {

  const relativeFilePath = filePath.replace(projectRoot, '').replace(/^\//, ''),
    pathDepth = relativeFilePath.match(/\//gm);

  return {
    'relative': relativeFilePath,
    'ladder': pathDepth ? pathDepth.map((path) => { return '../' }).join('') : ''
  }

};

module.exports = relativePathLadder;