"user strict";

const gulp = require('gulp');

const
  fonts     = require('./fonts'),
  images    = require('./images'),
  scripts   = require('./scripts'),
  server    = require('./server'),
  styles    = require('./styles')
  templates = require('./templates');


/**
 * Watch doc files during development
 */

const watch = () => {

  gulp.watch(
    './src/**/*.pug',
    gulp.series(
      templates,
      server().reload,
    )
  );

  gulp.watch(
    [
      './src/assets/styles/**/*.scss',
    ],
    gulp.series(
      styles,
      server().reload,
    )
  );

  gulp.watch(
    [
      './src/assets/scripts/**/*.js',
    ],
    gulp.series(
      scripts,
      server().reload,
    )
  );

  gulp.watch(
    [
      './src/assets/images/**/*',
    ],
    images,
  );
  
  gulp.watch(
    [
      './src/assets/fonts/**/*',
    ],
    fonts,
  );

}

module.exports = watch;
