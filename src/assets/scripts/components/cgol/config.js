import get2DArrMax from '../../../scripts/utilities/get2DArrMax';
import getRandomInt from '../../../scripts/utilities/getRandomInt';

const 
  ALIVE = 1,
  CONTROLLER_NAME_LIST = ['toggleTimelapse', 'forward', 'randomise'],
  DEAD = 0,
  PATTERN_LIST = [
    {
      name: 'Mini Invaders',
      pattern(ctx) {
        return {
          cells: [
            [0, 2], [1, 1], [2, 2],
          ],
          minColSize: 3,
          minRowSize: 3,
        }
      },
    },
    {
      name: 'Pong Invaders',
      pattern(ctx) {
        return {
          cells: [
            [0, 0], [1, 1], [0, 2], [7, 7], [8, 7],
          ],
          minColSize: 9,
          minRowSize: 8,
        }
      },
    },
    {
      name: 'Pine Tree with Snow',
      pattern(ctx) {
        const
          colStart = 13,
          rowStart = 1,
          steps = 6;

        let cells = [];

        for (let step = 0; step < steps; step++) {
          const nw1 = ctx.diagonalLine(colStart - step, rowStart + (step * 4), 'north-west', 4, ALIVE);
          const we1 = ctx.straightLine(nw1.endPoint.col, nw1.endPoint.row, 'west-east', 3, ALIVE);

          const ne1 = ctx.diagonalLine(colStart + step, rowStart + (step * 4), 'north-east', 4, ALIVE);
          const ew1 = ctx.straightLine(ne1.endPoint.col, ne1.endPoint.row, 'east-west', 3, ALIVE);

          cells.push(...nw1.cells, ...we1.cells, ...ne1.cells, ...ew1.cells);

          if (step == steps - 1) {
            const we2 = ctx.straightLine(we1.endPoint.col, we1.endPoint.row, 'west-east', 5, ALIVE);
            const ns1 = ctx.straightLine(we2.endPoint.col, we2.endPoint.row, 'north-south', 3, ALIVE);
            const we3 = ctx.straightLine(ns1.endPoint.col, ns1.endPoint.row, 'west-east', 3, ALIVE);

            const ew2 = ctx.straightLine(ew1.endPoint.col, ew1.endPoint.row, 'east-west', 5, ALIVE);
            const ns2 = ctx.straightLine(ew2.endPoint.col, ew2.endPoint.row, 'north-south', 3, ALIVE);
            const ew3 = ctx.straightLine(ns2.endPoint.col, ns2.endPoint.row, 'east-west', 3, ALIVE);

            cells.push(...we2.cells, ...ns1.cells, ...we3.cells, ...ew2.cells, ...ns2.cells, ...ew3.cells);
          }
        }

        cells.push(...[[4, 1], [8, 2], [2, 4], [5, 6], [1, 9], [5, 13], [1, 17], [3, 22], [1, 26]]);
        cells.push(...[[21, 0], [24, 3], [19, 5], [26, 7], [23, 10], [25, 14], [21, 15], [26, 20], [23, 25]]);
        cells.push(...[[13, 2], [0, 25], [3, 27], [4, 26], [6, 27], [7, 27], [8, 26], [17, 26], [18, 27], [19, 26], [20, 27], [21, 27], [25, 27], [26, 26], [27, 27]]);

        const
          minColSize = get2DArrMax(cells, 0) + 1,
          minRowSize = get2DArrMax(cells, 1) + 1;

        return {
          cells: cells,
          minColSize: minColSize,
          minRowSize: minRowSize,
        }
      }
    },
  ];

export { 
  ALIVE, 
  CONTROLLER_NAME_LIST,
  DEAD,
  PATTERN_LIST,
};