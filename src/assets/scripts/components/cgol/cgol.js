import { 
  ALIVE, DEAD, CONTROLLER_NAME_LIST, PATTERN_LIST, 
} from './config';
import createEl from '../../utilities/createEl';
import get2DArrMax from '../../utilities/get2DArrMax';
import getRandomInt from '../../utilities/getRandomInt';

/**
 * Game of life
 */
class CGOL {
  /**
   * @param {object} obj - Configuration yo
   * @param {string} obj.id - Canvas ID
   * @param {number=} obj.width - Canvas width
   * @param {number=} obj.height - Canvas height
   * @param {number=} obj.squareUnit - Square unit
   * @param {number=} obj.randomCells - Total number of random cells to be generated
   * @param {string=} obj.pattern - Name of the pattern to be rendered on the canvas
   * @param {number=} obj.delta - Timelapse speed
   * @param {boolean=} obj.dev - Helpful messages or feature such as canvas mapping for development purposes
   * 
   */
  constructor({
    id,
    width = 30,
    height = 30,
    squareUnit = 10,
    randomCells,
    pattern,
    delta = 100,
    dev = false,
  } = {}) {
    /** @private */
    this.id = id;
    /** @private */
    this.width = width;
    /** @private */
    this.height = height;
    /** @private */
    this.squareUnit = squareUnit;
    /** @private */
    this.randomCells = randomCells;
    /** @private */
    this.pattern = pattern;
    /** @private */
    this.delta = delta;
    /** @private */
    this.dev = dev;

    this.init();
  }

  /**
   * Additional preparation for starting the game
   */
  init() {
    CGOL.handleControlRoom = this.handleControlRoom.bind(this);

    this.cols = this.width / this.squareUnit;
    this.rows = this.height / this.squareUnit;
    this.area = this.createCellList();
    this.futureArea = this.getFutureCellList();
    this.hasFuture = true;
    this.isPaused = true;
    this.oldTime = 0;
    this.$controllers = {};

    if (this.pattern) {
      this.setAreaPattern(this.pattern);
    }

    this.setupCanvas();
    this.drawArea();
    this.mapCanvas();
    this.createControlRoom();
  }

  /**
   * Create initial cell list with dead cells
   */
  createCellList() {
    let cellList = [];

    for (let col = 0; col < this.cols; col++) {
      for (let row = 0; row < this.rows; row++) {
        cellList.push({
          pos: {
            col: col,
            row: row,
          },
          state: DEAD,
        });
      }
    }
    return cellList;
  }

  /**
   * Get total number of lives of an area(cell list)
   * @param {object} area - The area to get total number of lives from
   */
  getTotalNumberOfLives(area) {
    area = area ? area : this.area;

    return area.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.state;
    }, 0);
  }

  /** 
   * Get particular index of a cell in the list
   * @param {number} col - The column position
   * @param {number} row - The row position
   */
  getCellIndex(col, row) {
    return this.area.findIndex(item => {
      return item.pos.col === col && item.pos.row === row;
    });
  }

  /** 
   * Get the state of a particular cell
   * @param {number} col - The column position
   * @param {number} row - The row position
   */
  getCellState(col, row) {
    return this.area[this.getCellIndex(col, row)].state;
  }

  /** 
   * Set the state of a particular cell
   * @param {number} col - The column position
   * @param {number} row - The row position
   * @param {0|1} state - Alive = 1, Dead = 0
   */
  setCellState(col, row, state) {
    const index = this.getCellIndex(col, row);

    if (index >= 0) {
      this.area[index].state = state;
    } else {
      console.warn(`（　ﾟДﾟ）Can't set cell state since the provided cell position (${col}, ${row}) is out of bound`);
    }
  }

  /**
   * Bring certain number of random cells to life
   * @param {number} totalLives - The number of cells to be brought to life
   */
  bringRandomCellsToLife(totalLives) {
    this.wipeOutAllLife();

    for (let i = 0; i < totalLives; i++) {
      const
        randomCol = getRandomInt(this.cols),
        randomRow = getRandomInt(this.rows);
      
      if (this.getCellState(randomCol, randomRow)) {
        totalLives += 1;
        continue;
      }

      this.setCellState(randomCol, randomRow, ALIVE);
    }
  }

  /**
   * Set area population with a specific pattern
   * @param {string} name - The name of the pattern
   */
  setAreaPattern(name) {
    const 
      patternObj = CGOL.getPattern(name),
      pattern = patternObj.pattern(CGOL),
      patternMinColSize = pattern.minColSize, 
      patternMinRowSize = pattern.minRowSize;
    
    if (this.cols < patternMinColSize || this.rows < patternMinRowSize) {
      console.warn(`${name} requires minimum width of ${patternMinColSize * this.squareUnit} and height of ${patternMinRowSize * this.squareUnit}`);
      return;
    }

    for (let i in pattern.cells) {
      this.setCellState(pattern.cells[i][0], pattern.cells[i][1], ALIVE);
    }
  }

  /**
   * Wipe out all life of the current area
   */
  wipeOutAllLife() {
    for (let i in this.area) {
      const cell = this.area[i];

      if (cell.state) {
        cell.state = DEAD;
      }
    }
  }

  /**
   * Get total number of living neighbours of a specific cell
   * @param {number} col - The column position
   * @param {number} row - The row position
   */
  getTotalLivingNeighbours(col, row) {
    let totalLivingNeighbours = 0;

    for (let x = -1; x < 2; x++ ) {
      for (let y = -1; y < 2; y++ ) {
        const 
          neighbourCol = (col + (x) + this.cols) % this.cols,
          neighbourRow = (row + (y) + this.rows) % this.rows;
          
        // Ignore if neighbour position is pointing to the referenced cell
        if (neighbourCol === col && neighbourRow === row) {
          continue;
        }
          
        totalLivingNeighbours += this.getCellState(neighbourCol, neighbourRow);
      }
    }

    return totalLivingNeighbours;
  }

  /**
   * Find out the immediate fate of a specific cell
   * @param {number} col - The column position
   * @param {number} row - The row position
   */
  getCellNextFate(col, row) {
    const totalLivingNeighbours = this.getTotalLivingNeighbours(col, row);

    if (this.getCellState(col, row)) {
      if (totalLivingNeighbours < 2) {
        return {
          state: DEAD,
          reason: 'underpopulation', 
        }
      } else if (totalLivingNeighbours > 3) {
        return {
          state: DEAD,
          reason: 'overpopulation',
        }
      } else {
        return {
          state: ALIVE,
          reason: 'lives on',
        }
      }
    } else {
      if (totalLivingNeighbours == 3) {
        return {
          state: ALIVE,
          reason: 'reproduction',
        }
      } else {
        return {
          state: DEAD,
          reason: 'unideal neighbours',
        }
      }
    }
  }

  /**
   * Get the future cell list based on the referenced area
   * @param {object} area - The referenced area 
   */
  getFutureCellList(area) {
    area = area ? area : this.area;

    if (this.getTotalNumberOfLives(area) < 1) { return; }

    let futureArea = [];

    for (let i in area) {
      const 
        cell = area[i],
        col = cell.pos.col,
        row = cell.pos.row,
        nextFate = this.getCellNextFate(col, row);

      let futureCell = {
        pos: {
          col: col,
          row: row,
        },
        state: nextFate.state,
        history: ('history' in cell) ? cell.history : [],
      }

      futureCell.history.push({
        state: cell.state,
        reason: nextFate.reason,
      });

      futureArea.push(futureCell);
    }

    return futureArea;
  }

  /**
   * Check whether an area has a future or not by comparing the current and future
   * @param {object} current - Current area
   * @param {object} future - Future area
   */
  checkFutureCondition(current, future) {
    if (current.length !== future.length) { return; }

    let hasFuture = false;

    for (let i = 0; i < current.length; i++) {
      const 
        currentCell = `${current[i].pos.col}, ${current[i].pos.row}, ${current[i].state}`,
        futureCell = `${future[i].pos.col}, ${future[i].pos.row}, ${future[i].state}`;

      if (futureCell !== currentCell) { 
        hasFuture = true;
        break;
      }
    }

    return hasFuture;
  }

  /**
   * Create next area if it has a future
   */
  createNextArea() {
    const futureArea = this.getFutureCellList();

    if (!futureArea) { 
      this.hasFuture = false;
      return; 
    }

    this.hasFuture = this.checkFutureCondition(this.area, futureArea);
    
    if (!this.hasFuture) { return; }

    this.area = futureArea;
  }

  /**
   * Setup canvas based on user configuration if any.
   * Otherwise, will be using default values
   */
  setupCanvas() {
    this.canvas = document.getElementById(this.id);

    if (this.canvas) {
      this.canvas.width = this.width;
      this.canvas.height = this.height;
    }
  }

  /**
   * Draw area on canvas
   * @param {string=} map - Provide for mapping canvas in development mode
   */
  drawArea(map) {
    const canvas = map ? document.getElementById(`${this.id}-mapCanvas`) : this.canvas;

    if (!canvas) {
      console.log('（　ﾟДﾟ）No canvas found');
      return;
    }

    if (!canvas.getContext) {
      console.log('（　ﾟДﾟ）Your browser do not support canvas');
      return;
    }

    let mapOffset = 1;

    if (map && this.squareUnit == 10) {
      mapOffset = 2;
    }

    let ctx = canvas.getContext('2d');

    this.clearCanvas(canvas, ctx);

    for (let i in this.area) {
      const 
        cell = this.area[i],
        col = cell.pos.col,
        row = cell.pos.row,
        state = cell.state,
        shape = new Path2D(),
        cellID = `${col},${row}`;

      let color;

      if (this.dev) {
        color = '200, 0, 0';
      } else {
        color = '255, 255, 255';
      }
      
      ctx.fillStyle = `rgba(${color}, 0.5)`;

      if (map) {
        const fontSize = Math.floor(8);
        ctx.font = `${fontSize}px serif`;
        ctx.fillText(cellID, (col * (this.squareUnit * mapOffset)) + (this.squareUnit * mapOffset / 3.5), (row * (this.squareUnit * mapOffset)) + ((this.squareUnit * mapOffset) - (this.squareUnit * mapOffset) / 3.5));
        ctx.fillStyle = `rgba(${color}, 0.2)`;
      }

      if (state) {
        shape.rect(col * (this.squareUnit * mapOffset), row * (this.squareUnit * mapOffset), (this.squareUnit * mapOffset), (this.squareUnit * mapOffset));
        ctx.fill(shape);
        ctx.fillStyle = `rgba(${color}, 0.5)`;
      }
    }
  }

  /**
   * Clear canvas
   * @param {object} canvas - The canvas to clear
   * @param {object} ctx - The canvas context
   */
  clearCanvas(canvas, ctx) {
    if (!ctx) {
      ctx = canvas.getContext('2d');
    }

    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }

  /**
   * Map canvas for development purposes. 
   * Works when `dev` config is set to true.
   */
  mapCanvas() {
    if (!this.dev) { return; }

    let
      mapCanvasID = `${this.id}-mapCanvas`, 
      mapCanvas = document.getElementById(mapCanvasID);

    if (!this.canvas) {
      console.log(`（　ﾟДﾟ）No canvas found to be mapped`);
      return;
    }
    
    if (!mapCanvas) {
      let canvasOffset = 1;
  
      if (this.squareUnit == 10) {
        canvasOffset = 2;
      }
  
      mapCanvas = createEl({
        tag: 'canvas',
        attr: {
          id: mapCanvasID,
          width: this.width * canvasOffset,
          height: this.height * canvasOffset,
        },
      });

      this.canvas.before(mapCanvas);
    }

    this.drawArea('map');
  }

  /**
   * Create control room for visualisations
   */
  createControlRoom() {
    if (!this.canvas) { return; }
    
    let controlRoomWrapper = document.querySelector(`[data-cgol-control-room='${this.id}']`);
    
    if (controlRoomWrapper) {
      return;
    }
    
    controlRoomWrapper = createEl({
      tag: 'div',
      attr: {
        'data-cgol-control-room': this.id,
        class: 'CGOL-control-room',
      }
    });
    
    controlRoomWrapper.appendChild(this.createControllers());
    this.canvas.after(controlRoomWrapper);

    controlRoomWrapper.addEventListener('click', CGOL.handleControlRoom);
  }

  /**
   * Create controllers
   */
  createControllers() {
    const controllerGroupWrapper = createEl({
      tag: 'div',
      attr: {
        'data-cgol-controller-group': this.id,
        class: 'CGOL-controller-group',
      }
    });

    CONTROLLER_NAME_LIST.forEach(controllerName => {
      const controller = createEl({
        tag: 'button',
        attr: {
          type: 'button',
          'data-cgol-controller': controllerName,
          'data-cgol-action': '',
          class: 'CGOL-controller',
        },
        content: `<span class="CGOL-controller__text _CGOL-visually-hidden">${(controllerName === 'toggleTimelapse' ? 'play' : controllerName)}</span>`,
      });

      this.$controllers[controllerName] = controller;
      controllerGroupWrapper.appendChild(controller);
    });

    return controllerGroupWrapper;
  }

  /**
   * Handle controllers' event
   */
  handleControlRoom() {
    let 
      target = event.target,
      isController = target.nodeName.toLowerCase() === 'button';

    const attrController = 'data-cgol-controller';

    if (!isController) {
      const controller = target.closest(`[${attrController}]`);

      if (!controller) { return; }

      isController = true;
      target = controller;
    } 

    const name = target.getAttribute(attrController);

    if ((name === 'toggleTimelapse' || name === 'forward') && !this.hasFuture) {
      return;
    }
    
    this[name]();
  }

  /**
   * Draw immediate future area on canvas
   */
  forward() {
    if (this.dev) { CGOL.devLog('forward()', '[Start]') }

    this.createNextArea();

    if (!this.hasFuture) { 
      if (this.oldSchoolLoop) {
        clearInterval(this.oldSchoolLoop);
      }
      return; 
    }

    this.drawArea();
    this.mapCanvas();
  }

  /**
   * Generate random population on canvas
   */
  randomise() {
    if (this.dev) { CGOL.devLog('randomise()', '[Start]') }

    let totalCells = this.randomCells ? this.randomCells : Math.floor((this.cols * this.rows) * 20 / 100);
    
    this.bringRandomCellsToLife(totalCells);
    this.pioneer = this.area;

    this.hasFuture = true;
    
    this.drawArea();
    this.mapCanvas();
  }

  /**
   * Start or pause timelapse
   */
  toggleTimelapse() {
    if (this.dev) { CGOL.devLog('toggleTimelapse()', '[Start]') }

    if (this.isPaused) {
      this.playback();
    } else {
      this.pausePlayback();
    }
  }

  /**
   * Start timelapse
   */
  playback() {
    this.isPaused = false;
    this.loopRequest = window.requestAnimationFrame(this.loop.bind(this));

    if ('toggleTimelapse' in this.$controllers) {
      this.$controllers.toggleTimelapse.querySelector('.CGOL-controller__text').innerHTML = 'pause';
      this.$controllers.toggleTimelapse.setAttribute('data-cgol-action', 'pause');
    }
  }

  /**
   * Pause timelapse
   */
  pausePlayback() {
    this.isPaused = true;
    window.cancelAnimationFrame(this.loopRequest);

    if ('toggleTimelapse' in this.$controllers) {
      this.$controllers.toggleTimelapse.querySelector('.CGOL-controller__text').innerHTML = 'play';
      this.$controllers.toggleTimelapse.setAttribute('data-cgol-action', 'play');
    }
  }

  /**
   * Handle loop with controlled speed
   */
  loop(currentTime) {
    if (!this.hasFuture || this.isPaused) {
      this.pausePlayback();
      return;
    }

    if (this.oldTime === 0) {
      this.oldTime = currentTime;
    }

    if ((currentTime - this.oldTime) >= this.delta) {
      this.forward();
      this.oldTime = currentTime;
    }

    if (!window) {
      if (!this.oldSchoolLoop) {
        this.oldSchoolLoop = setInterval(() => { 
          this.forward();
        }, 150);
      }
    } else {
      this.loopRequest = window.requestAnimationFrame(this.loop.bind(this));
    }
  };

  /**
   * Create straight line
   * @param {number} col - Starting col
   * @param {number} row - Starting row
   * @param {'west-east'|'east-west'|'north-south'|'south-north'} direction - Direction from starting point
   */
  static straightLine(col, row, direction, length, state) {
    let
      cells = [],
      endPoint = {};

    for (let i = 0; i < length; i++) {
      let colPos, rowPos;

      if (direction === 'west-east') {
        colPos = col + i;
        rowPos = row;
      } if (direction === 'east-west') {
        colPos = col - i;
        rowPos = row;
      } if (direction === 'north-south') {
        colPos = col;
        rowPos = row + i;
      } if (direction === 'south-north') {
        colPos = col;
        rowPos = row - i;
      }

      cells.push([colPos, rowPos]);

      if (i == length - 1) {
        endPoint.col = colPos;
        endPoint.row = rowPos;
      }
    }

    return {
      cells: cells,
      endPoint: endPoint,
    }
  }

  /**
   * Create diagonal line
   * @param {number} col - Starting col
   * @param {number} row - Starting row
   * @param {'north-east'|'north-west'|'south-east'|'south-west'} direction - Direction from starting point
   */
  static diagonalLine(col, row, direction, length, state) {
    let
      cells = [],
      endPoint = {};

    for (let i = 0; i < length; i++) {
      let colPos, rowPos;

      if (direction === 'north-east') {
        colPos = col + i;
        rowPos = row + i;
      } else if (direction === 'north-west') {
        colPos = col - i;
        rowPos = row + i;
      } else if (direction === 'south-east') {
        colPos = col + i;
        rowPos = row - i;
      } else if (direction === 'south-west') {
        colPos = col - i;
        rowPos = row - i;
      }

      cells.push([colPos, rowPos]);

      if (i == length - 1) {
        endPoint.col = colPos;
        endPoint.row = rowPos;
      }
    }

    return {
      cells: cells,
      endPoint: endPoint,
    }
  }

  /**
   * Helper for dev mode
   * @param {string} msg - The message
   * @param {string} prefix - Prefix
   */
  static devLog(msg, prefix) {
    if (prefix) {
      console.log(`-- ${prefix} ${msg} --`);
    } else {
      console.log(`${msg}`);
    }
  }

  /**
   * Helper to retrieve pattern object
   * @param {name} - The name of the pattern
   */
  static getPattern(name) {
    const [pattern] = PATTERN_LIST.filter(pattern => {
      return pattern.name.toLowerCase() === name.toLowerCase();
    });
    return pattern;
  }
}

export default CGOL;