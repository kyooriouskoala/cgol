"use strict";

const gulpConfig = require('./gulp/config');

const gulp = require('gulp');

const
  clean = require('./gulp/tasks/clean'),
  fonts = require('./gulp/tasks/fonts'),
  images = require('./gulp/tasks/images'),
  scripts = require('./gulp/tasks/scripts'),
  server = require('./gulp/tasks/server'),
  styles = require('./gulp/tasks/styles'),
  templates = require('./gulp/tasks/templates'),
  watch = require('./gulp/tasks/watch');

const buildPath = `./${gulpConfig.buildDir}/**/*`;

/**
 * Development
 */

exports.dev = gulp.series(
  clean({ paths: buildPath }),
  templates,
  fonts,
  images,
  styles,
  scripts,
  server().init,
  watch,
);

/**
 * Production
 */

exports.prod = gulp.series(
  clean({ paths: buildPath }),
  templates,
  fonts,
  images,
  styles,
  scripts,
);