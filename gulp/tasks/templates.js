"use strict";

const gulpConfig = require('../config');

const relativePathLadder = require('../utilities/relativePathLadder');

const 
  gulp = require('gulp'),
  pug = require('gulp-pug'),
  $_ = {
    cached: require('gulp-cached'),
    data: require('gulp-data'),
    if: require('gulp-if'),
  };

/**
 * Compile template
 */

const templates = () => {
  return gulp.src([
    './src/**/*.pug',
    '!./src/**/_*.pug',
  ])
    .pipe($_.if(
      !gulpConfig.isProduction,
      $_.cached('pages', { optimizeMemory: true })
    ))
    .pipe($_.data((file) => {
      return {
        environment: gulpConfig.isProduction ? 'public' : 'dev',
        pathLadder: relativePathLadder(file.path, `${gulpConfig.projectRoot}/src/`, '.pug').ladder.replace(/^..\//, ''),
      }
    }))
    .pipe(pug({
      pretty: true,
      basedir: 'src',
    }))
    .pipe(gulp.dest(`./${gulpConfig.buildDir}/`));
}

module.exports = templates;

