import CGOL from '../cgol';

window.addEventListener('DOMContentLoaded', () => {
  const cgol = new CGOL({
    id: 'universe',
    width: 90,
    height: 90,
    pattern: 'Pong Invaders',
    dev: true,
  });
});