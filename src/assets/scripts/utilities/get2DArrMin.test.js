import get2DArrMin from './get2DArrMin';

const data = {
  one: {
    arr: [
      [8, 2],
      [1, 1],
      [2, 6],
    ],
    min0: 1,
    min1: 1,
  },
  two: {
    arr: [
      [-8, 2],
      [1, -1],
      [2, 6],
    ],
    min0: -8,
    min1: -1,
  },
  three: {
    arr: [
      [-8, -2],
      [-1, -9],
      [-2, -6],
    ],
    min0: -8,
    min1: -9,
  } 
}

test(
  `Should return smallest number in data.one nested array at index 0`, 
  () => {
    expect(get2DArrMin(data.one.arr, 0)).toEqual(data.one.min0);
  }
)

test(
  `Should return smallest number in data.one nested array at index 1`, 
  () => {
    expect(get2DArrMin(data.one.arr, 1)).toEqual(data.one.min1);
  }
)

test(
  `Should return smallest number in data.two nested array at index 0`, 
  () => {
    expect(get2DArrMin(data.two.arr, 0)).toEqual(data.two.min0);
  }
)

test(
  `Should return smallest number in data.two nested array at index 1`, 
  () => {
    expect(get2DArrMin(data.two.arr, 1)).toEqual(data.two.min1);
  }
)

test(
  `Should return smallest number in data.three nested array at index 0`, 
  () => {
    expect(get2DArrMin(data.three.arr, 0)).toEqual(data.three.min0);
  }
)

test(
  `Should return smallest number in data.three nested array at index 1`, 
  () => {
    expect(get2DArrMin(data.three.arr, 1)).toEqual(data.three.min1);
  }
)