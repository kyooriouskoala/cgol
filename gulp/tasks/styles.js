"use strict";

const gulpConfig = require('../config');

const
  $_ = {
    autoprefixer: require('gulp-autoprefixer'),
    if: require('gulp-if'),
    rename: require('gulp-rename'),
    sass: require('gulp-sass'),
    sourcemaps: require('gulp-sourcemaps'),
  },
  gulp = require('gulp');

/**
 * Compile styles
 */

const styles = () => {
  return gulp.src([
    `./src/assets/styles/**/*.scss`,
  ])
    .pipe($_.if(
      !gulpConfig.isProduction,
      $_.sourcemaps.init()
    ))
    .pipe($_.sass({
      outputStyle: gulpConfig.isProduction ? 'compressed' : 'expanded',
      errorLogToConsole: true,
    }))
    .on('error', $_.sass.logError)
    //.on('error', console.error.bind(console))
    .pipe($_.autoprefixer({
      Browserslist: ['last 2 versions'],
      cascade: false,
    }))
    .pipe($_.if(
      gulpConfig.isProduction,
      $_.rename({ suffix: '.min' })
    ))
    .pipe($_.if(
      !gulpConfig.isProduction,
      $_.sourcemaps.write('./')
    ))
    .pipe(gulp.dest(`./${gulpConfig.buildDir}/assets/styles/`));
};

module.exports = styles;