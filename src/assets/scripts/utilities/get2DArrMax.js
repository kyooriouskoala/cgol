const get2DArrMax = (arr, idx) => {
  /* const values = arr.map(function (val) { return val[idx]; });
  return Math.max(...values); */
  
  return arr.reduce((prev, curr) => {
    return (prev <= curr[idx]) ? curr[idx] : prev;
  }, arr[0][idx]);
}

export default get2DArrMax;