import get2DArrMax from './get2DArrMax';

const data = {
  one: {
    arr: [
      [8, 2],
      [1, 1],
      [2, 6],
    ],
    max0: 8,
    max1: 6,
  },
  two: {
    arr: [
      [-8, 2],
      [1, -1],
      [2, 6],
    ],
    max0: 2,
    max1: 6,
  },
  three: {
    arr: [
      [-8, -2],
      [-1, -9],
      [-2, -6],
    ],
    max0: -1,
    max1: -2,
  } 
}

test(
  `Should return biggest number in data.one nested array at index 0`, 
  () => {
    expect(get2DArrMax(data.one.arr, 0)).toEqual(data.one.max0);
  }
)

test(
  `Should return biggest number in data.one nested array at index 1`, 
  () => {
    expect(get2DArrMax(data.one.arr, 1)).toEqual(data.one.max1);
  }
)

test(
  `Should return biggest number in data.two nested array at index 0`, 
  () => {
    expect(get2DArrMax(data.two.arr, 0)).toEqual(data.two.max0);
  }
)

test(
  `Should return biggest number in data.two nested array at index 1`, 
  () => {
    expect(get2DArrMax(data.two.arr, 1)).toEqual(data.two.max1);
  }
)

test(
  `Should return biggest number in data.three nested array at index 0`, 
  () => {
    expect(get2DArrMax(data.three.arr, 0)).toEqual(data.three.max0);
  }
)

test(
  `Should return biggest number in data.three nested array at index 1`, 
  () => {
    expect(get2DArrMax(data.three.arr, 1)).toEqual(data.three.max1);
  }
)