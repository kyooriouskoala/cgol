"use strict";

const gulpConfig = require('../config');

const browserSync = require('browser-sync');

const projectServer = browserSync.create(gulpConfig.browserSync.name);

/**
 * Local server and live reload
 */

const server = () => {
  return {
    init: function (done) {
      projectServer.init({
        open: false,
        injectChanges: true,
        server: 'dev',
      });

      done();
    },

    reload: function (done) {
      browserSync.get(gulpConfig.browserSync.name).reload();
      done();
    }
  }
}

module.exports = server;
