"use strict";

const gulpConfig = require('../config');

const
  gulp = require('gulp'),
  $_ = {
    if: require('gulp-if'),
    imagemin: require('gulp-imagemin'),
  };

/**
 * Copy images and optimise for production build
 */

const images = () => {
  return gulp.src([
    `./src/assets/images/**/*`,
  ])
    .pipe($_.if(
      gulpConfig.isProduction,
      $_.imagemin()
    ))
    .pipe(gulp.dest(`./${gulpConfig.buildDir}/assets/images/`));
}

module.exports = images;