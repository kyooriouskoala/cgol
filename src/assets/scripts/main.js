import CGOL from './components/cgol/cgol';

window.addEventListener('DOMContentLoaded', () => {
  const cgol = new CGOL({
    id: 'universe',
    width: 280,
    height: 280,
    pattern: 'Pine Tree with Snow',
  });
});