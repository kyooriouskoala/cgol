/**
 * Create element
 * @param {object} obj - Configuration
 * @param {tag} obj.tag - Tag name
 * @param {content=} obj.content - Element content
 * @param {object=} obj.attr - Object containing attributes
 */
const createEl = (obj) => {
  let el;

  el = document.createElement(obj.tag);

  if (obj.content) {
    el.innerHTML = obj.content;
  }

  for (const PROPERTY in obj.attr) {
    el.setAttribute(PROPERTY, obj.attr[PROPERTY]);
  }

  return el;
}

export default createEl;