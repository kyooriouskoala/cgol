"use strict";

const gulpConfig = require('../config');

const gulp = require('gulp');

/**
 * Copy fonts to build
 */

const fonts = () => {
  return gulp.src([
    `./src/assets/fonts/**/*`,
  ])
    .pipe(gulp.dest(`./${gulpConfig.buildDir}/assets/fonts/`));
}

module.exports = fonts;