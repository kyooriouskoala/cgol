const get2DArrMin = (arr, idx) => {
  /* const values = arr.map(function (val) { return val[idx]; });
  return Math.min(...values); */

  return arr.reduce((prev, curr) => {
    return (prev >= curr[idx]) ? curr[idx] : prev;
  }, arr[0][idx]);
}

export default get2DArrMin;